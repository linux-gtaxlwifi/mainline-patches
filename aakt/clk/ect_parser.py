def parse_ect_text(text):
    outermost = [{}]
    stack = [(0, outermost)]
    for i in text.strip().split('\n'):
        offset = i.find('[')
        assert offset >= 0
        assert not i[:offset].strip()
        assert '] : ' in i
        key, value = i[offset+1:].split('] : ')
        if offset > stack[-1][0]:
            new = stack[-1][1][-1]['children'] = [{}]
            stack.append((offset, new))
        elif offset < stack[-1][0]:
            while offset < stack[-1][0]:
                stack.pop()
            stack[-1][1].append({})
        if key in stack[-1][1][-1]:
            stack[-1][1].append({})
        stack[-1][1][-1][key] = value
    data, = outermost
    data, = data['children']
    n_plls = int(data['NUM OF PLL'])
    assert len(data['children']) == n_plls
    ans = {}
    for i in data['children']:
        key = i['PLL NAME']
        n_freq = int(i['NUM OF FREQUENCY'])
        assert len(i['children']) == n_freq, (n_freq, i['children'])
        frequencies = [{a: int(i[b]) for a, b in dict(rate='FREQUENCY', pdiv='P', mdiv='M', sdiv='S', kdiv='K').items()} for i in i['children']]
        ans[key] = frequencies
    return ans

def parse_name(blob, offset):
    namelen = int.from_bytes(blob[offset:offset+4], 'little') + 1
    name = blob[offset+4:offset+4+namelen].rstrip(b'\0')
    offset += 4 + namelen + (-namelen) % 4
    return name, offset

def parse_pll_binary(blob, offset):
    offset0 = offset
    npll = int.from_bytes(blob[offset+8:offset+12], 'little')
    offset += 12
    ans = {}
    for idx in range(npll):
        name, offset = parse_name(blob, offset)
        block_offset = offset0 + int.from_bytes(blob[offset:offset+4], 'little')
        offset += 4
        nfreq = int.from_bytes(blob[block_offset+4:block_offset+8], 'little')
        freqs = []
        for i in range(nfreq):
            freq = {}
            for j, k in enumerate(('rate', 'pdiv', 'mdiv', 'sdiv', 'kdiv')):
                freq[k] = int.from_bytes(blob[block_offset+8+20*i+4*j:block_offset+12+20*i+4*j], 'little')
            freqs.append(freq)
        ans[name.decode('ascii')] = freqs
    return ans

def parse_ect_binary(blob):
    assert blob.startswith(b'PARA')
    n_headers = int.from_bytes(blob[12:16], 'little')
    offset = 16
    while True:
        name, offset = parse_name(blob, offset)
        block_offset = int.from_bytes(blob[offset:offset+4], 'little')
        if name == b'PLL':
            return parse_pll_binary(blob, block_offset)
        offset += 4
    assert False

def parse_ect(blob):
    if b'\0' in blob:
        return parse_ect_binary(blob)
    else:
        return parse_ect_text(blob.decode('ascii'))
