import sys, json, collections, time, os

# step 0a. verify arguments

if len(sys.argv) != 7:
    print('''\
usage: dump2mainline <dump.json> <v2psfrmap.c> <driver.c> <dt-bindings.h> <dt-fragment.dtsi> <soc.json>

Inputs:
- dump.json -- data file with clock info
- v2psfrmap.c -- vendor source file that contains the v2psfrmap array
- soc.json -- JSON with configuration data (see example)

Outputs:
- driver.c -- file that should be placed at drivers/clk/samsung/clk-{soc}.c
- dt-bindings.h -- file that should be placed at include/dt-bindings/clock/{soc}.h
- dt-fragment.dtsi -- DTB source fragment with clock definitions

Environment variables:
- DEVICETREE=0 -- output devicetree for inclusion in the mainline kernel
- DEVICETREE=1 -- output the same devicetree, but with defines already substituted
- DEVICETREE=2 -- also output the fixed clock dependencies
''')
    exit(0)

nr_devicetree = int(os.getenv('DEVICETREE', '0'))

# step 0b. read clock data. we don't need the vclk stuff, only clocks

with open(sys.argv[1]) as file:
    data = json.load(file)

clocks = {i['name']: i for i in data['clocks']}
vclocks = {i['name']: i for i in data['virtual_clocks']}

with open(sys.argv[6]) as file:
    config = json.load(file)

soc_name = config['soc']
copyright_year = config['copyright_year']

# step 0c. parse clock bank names from the header file

bank_names = {}
bank_addrs = {}

with open(sys.argv[2]) as file:
    for line in file:
        line = ' '.join(line.split())
        if line.startswith('DEFINE_V2P('):
            a, b = line.split('(', 1)[1].split(')', 1)[0].split(',')
            a = a.strip()
            b = int(b.strip(), 0)
            bank_names[b] = a
            bank_addrs[a] = b

# step 1. reparent gate clocks according to the vclk hierarchy

clk_refcount = collections.defaultdict(int)
vclk_refcount = collections.defaultdict(int)

for name, clock in clocks.items():
    for parent in clock.get('parents', [clock.get('parent', None)]):
        clk_refcount[parent] += 1

for vclk in data['virtual_clocks']:
    if vclk['type'] == 'grpgate':
        for i in vclk['gates']:
            vclk_refcount[i] += 1
    elif vclk['type'] == 'pxmxdx':
        for i in vclk['children']:
            vclk_refcount[i] += 1
    elif vclk['type'] in ('d1', 'p1', 'umux'):
        vclk_refcount[vclk['clk']] += 1
    elif vclk['type'] == 'm1d1g1':
        vclk_refcount[vclk['mux']] += 1
        vclk_refcount[vclk['div']] += 1
        vclk_refcount[vclk['gate']] += 1
        vclk_refcount[vclk['extmux']] += 1

subst_parent = {}

for vclk in data['virtual_clocks']:
    if vclk['type'] == 'grpgate':
        vparent = vclk
        if vclk['parent'] is None:
            clk_parents_group = {}
            for i in vclk['gates']:
                if clk_refcount[i] != 0 or vclk_refcount[i] != 1: continue
                if 'parent' not in clocks[i].keys() or clocks[i]['parent'] is None: continue
                if clocks[i]['parent'] not in clk_parents_group: clk_parents_group[clocks[i]['parent']] = []
                clk_parents_group[clocks[i]['parent']].append(i)
            for reparent, clks in clk_parents_group.items():
                for i in range(1, len(clks)):
                    subst_parent[clks[i]] = clks[i - 1]
            continue
        while vparent['parent'] is not None:
            vparent = vclocks[vparent['parent']]
            if vparent['type'] == 'm1d1g1':
                if vparent['extmux'] is not None: break
                assert clocks[vparent['gate']]['parent'] == vparent['div']
                assert clocks[vparent['div']]['parent'] == vparent['mux']
                vparent_gate = vparent['gate']
            elif vparent['type'] != 'grpgate': break
            elif len(vparent['gates']) != 1: continue
            else:
                vparent_gate = vparent['gates'][0]
            parent2 = clocks[vparent_gate]['parent']
            reparentable_clocks = []
            for i in vclk['gates']:
                if i not in subst_parent and clk_refcount[i] == 0 and vclk_refcount[i] == 1 and (clocks[i]['parent'] is None or clocks[i]['parent'] == parent2):
                    reparentable_clocks.append(i)
            for i in reparentable_clocks:
                subst_parent[i] = vparent_gate

for k, v in subst_parent.items():
    #print(k, '->', v)
    clocks[k]['parent'] = v

# step 2a. substitute clocks with their owning gate & mux clocks, if they exist. mainline clocks are ungated

subst = {}
subst_values = {}

for name, clock in clocks.items():
    assert not ('gate' in clock and 'mux' in clock)
    if 'enable' in clock and clock['enable'] is not None:
        assert 'gate' in clock and (clock['gate'] is not None or '_MUX_' in name), name
        if clock['gate'] is not None:
            assert clocks[clock['gate']]['main'] == clock['enable']
            if clocks[clock['gate']]['parent'] is None:
                clocks[clock['gate']]['parent'] = name
            assert clocks[clock['gate']]['parent'] == name
    if 'gate' in clock and clock['gate'] is not None:
        assert 'enable' not in clock or clocks[clock['gate']]['main'] == clock['enable']
        if 'parent' not in clock or clock['parent'] is None or 'gate' not in clocks[clock['parent']] or clocks[clock['parent']]['gate'] != clock['gate']:
            assert name not in subst and name not in subst_values
            subst[name] = clock['gate']
            subst_values[clock['gate']] = name
    if 'mux' in clock and clock['mux'] is not None:
        assert name not in subst and name not in subst_values
        subst[name] = clock['mux']
        subst_values[clock['mux']] = name
        assert clocks[clock['mux']]['parents'][1] == name
        clocks[clock['mux']]['is_umux'] = True
        clocks[clock['mux']]['parent'] = name

# step 2b. identify "umux" clocks

for vclk in data['virtual_clocks']:
    if vclk['type'] == 'umux':
        assert clocks[vclk['clk']]['type'] == 'mux'
        clocks[vclk['clk']]['is_umux'] = True

# step 2c. create gate clocks for gated clocks that don't have a gate attached

for name, clock in list(clocks.items()):
    if 'enable' in clock and clock['enable'] is not None and clock['gate'] is None:
        gate_name = name.replace('_MUX_', '_MUXGATE_', 1)
        assert gate_name not in clocks
        clocks[gate_name] = {
            'type': 'gate',
            'name': gate_name,
            'parent': name,
            'main': clock['enable'],
        }
        assert name not in subst
        if name in subst_values:
            assert subst_values[name] != name
            subst[subst_values[name]] = gate_name
        subst[name] = gate_name
        subst_values[gate_name] = name

# step 3a. sanitize registers

for name, clock in clocks.items():
    regs = [i for i in (clock.get(i, None) for i in ('main', 'status', 'enable')) if i is not None]
    assert len({i['addr'] - i['offset'] for i in regs}) <= 1

# step 3b. identify clocks belonging to each bank

def get_clock_reg(clock):
    # does this clock itself have a register?
    if 'main' in clock:
        return clock['main']
    # otherwise, try its parents
    while 'parent' in clock and clock['parent'] is not None:
        clock = clocks[clock['parent']]
        if 'main' in clock:
            return clock['main']
    # now this must be a fixed rate. these do not belong to a bank
    assert clock['type'] == 'fixed_rate'
    return None

clocks_per_bank = collections.defaultdict(list)
clock_banks = {}

for name, clock in clocks.items():
    reg = get_clock_reg(clock)
    if reg is None:
        clock_banks[name] = None
    else:
        clock_banks[name] = bank_names[reg['addr'] - reg['offset']]
    clocks_per_bank[clock_banks[name]].append(name)

# step 3c. assign ordinals (indices)

clock_location = {}

for i, j in clocks_per_bank.items():
    j.sort()
    for idx, clk in enumerate(j):
        clock_location[clk] = (i, idx+1)

# step 4. identify inter-bank dependencies and parent clocks

bank_deps = collections.defaultdict(set)
bank_clk_deps = collections.defaultdict(set)

for name, clock in clocks.items():
    bank1 = clock_banks[name]
    if bank1 is None: continue
    for parent in clock.get('parents', [clock.get('parent', None)]):
        if parent is None: continue
        if parent in subst and subst[parent] != name:
            parent = subst[parent]
        bank2 = clock_banks[parent]
        if bank2 is not None and bank2 != bank1:
            bank_deps[bank1].add(bank2)
        if bank2 is None or bank2 != bank1:
            bank_clk_deps[bank1].add(parent)

for k in bank_clk_deps:
    bank_clk_deps[k] = sorted(bank_clk_deps[k])

# step 5. sort banks in top-sort order

sorted_banks = []

while len(sorted_banks) + 1 < len(clocks_per_bank):
    bb = []
    for bank in clocks_per_bank:
        if bank is not None and bank not in sorted_banks and bank_deps[bank].issubset(set(sorted_banks)):
            bb.append(bank)
    sorted_banks.extend(sorted(bb))

# step 6a. identify critical clocks

criticals = set()

for idx, i in enumerate(i['name'] for i in data['clocks']):
    if i not in config.get('non_critical_clocks', ()) and (idx < config.get('critical_first_n', 0) or i in config.get('critical_clocks', ())):
        criticals.add(i)
        if i in subst:
            criticals.add(subst[i])

# step 6b. identify which clocks should be present in the driver. this does not change the devicetree indices

if 'only_clocks' in config:
    queue = set()
    for i in config['only_clocks']:
        if isinstance(i, int):
            for j in data['clocks'][:i]:
                queue.add(j['name'])
        elif isinstance(i, str):
            queue.add(i)
    queue2 = set()
    clock_set = set()
    while queue:
        for i in queue:
            if i not in clock_set:
                clock_set.add(i)
                if i in subst:
                    queue2.add(subst[i])
                clock = clocks[i]
                if 'parent' in clock and clock['parent'] is not None:
                    queue2.add(clock['parent'])
                if 'parents' in clock:
                    queue2 |= set(clock['parents'])
                if 'gate' in clock and clock['gate'] is not None:
                    queue2.add(clock['gate'])
                if 'mux' in clock and clock['mux'] is not None:
                    queue2.add(clock['mux'])
        queue = queue2
        queue2 = set()
    clocks = {i: clocks[i] for i in clock_set}

# step 7. print the driver source

def get_mainline_bank_name(bank, cmu=True):
    if bank.endswith('_BASE'):
        bank = bank[:-5]
    if not cmu and bank.startswith('CMU_'):
        bank = bank[4:]
    return bank

def get_mainline_name(clock, self):
    name = clock['name']
    if name in subst and clock is not self and subst[name] != self['name'] and subst_values[subst[name]] != self['name']:
        name = subst[name]
        clock = clocks[name]
    bank = clock_location[name][0]
    assert name.startswith('clk_')
    name = name[4:]
    name = name.lower()
    if bank is not None:
        bank = get_mainline_bank_name(bank, cmu=False).lower()
        if name.startswith(bank+'_') and name != bank + '_pll':
            name = name[len(bank)+1:]
    if name.startswith('clk_'):
        name = name[4:]
    if clock['type'] == 'fixed_div':
        if name.startswith('ff_'):
            name = name[3:]
        name = 'ffac_' + name
    elif clock['type'] in ('pll_141xx', 'pll_1431x'):
        name = 'fout_' + name
    elif clock['type'] == 'mux':
        if name.startswith('mux_'):
            name = name[4:]
        name = 'mout_' + name
    elif clock['type'] == 'div':
        if name.startswith('div_'):
            name = name[4:]
        name = 'dout_' + name
    elif clock['type'] == 'gate':
        if name.startswith('gate_'):
            name = name[5:]
        name = 'gout_' + name
    elif clock['type'] == 'fixed_rate': pass
    else:
        assert False, (name, clock['type'])
    return name

def get_mainline_reg_name(clock):
    name = clock['name']
    bank = clock_location[name][0]
    assert name.startswith('clk_')
    name = name[4:]
    if name.startswith('CLK_'):
        name = name[4:]
    if bank is not None:
        bank = get_mainline_bank_name(bank, cmu=False)
        if name.startswith(bank+'_') and '_'+bank+'_' in name:
            name = name[len(bank)+1:]
    return name.upper()

with open(sys.argv[3], 'w') as file:
    file.write('''\
// SPDX-License-Identifier: GPL-2.0-only
/*
 * Copyright (C) '''+str(copyright_year)+''' Samsung Electronics Co., Ltd.
 * Copyright (C) '''+str(time.localtime().tm_year)+''' ???
 *
 * Common Clock Framework support for {Soc}.
 */

#include <linux/clk.h>
#include <linux/clk-provider.h>
#include <linux/of.h>
#include <linux/of_device.h>
#include <linux/platform_device.h>

#include <dt-bindings/clock/{soc}.h>

#include "clk.h"
#include "clk-exynos-arm64.h"

'''.format(soc=soc_name, Soc=soc_name[:1].upper()+soc_name[1:].lower()))
    for bank in sorted_banks:
        bank_name = get_mainline_bank_name(bank)
        bank_name_lower = get_mainline_bank_name(bank, cmu=False).lower()
        file.write('/* ---- '+bank_name+' '+'-'*(68-len(bank_name))+'*/\n')
        reg_defs = []
        ffactors = []
        plls = []
        mux_parents = []
        muxes = []
        divs = []
        gates = []
        for name0 in clocks_per_bank[bank]:
            if name0 not in clocks: continue
            clock = clocks[name0]
            is_critical = name0 in criticals
            name = get_mainline_name(clock, clock)
            if 'parent' in clock and clock['parent'] is not None:
                parent_name = get_mainline_name(clocks[clock['parent']], clock)
            else:
                parent_name = '!!MISSING!!'
            reg_name = get_mainline_reg_name(clock)
            fmt = dict(
                NAME = name.upper(),
                name = name,
                parent = parent_name,
                REG = reg_name,
                shift = clock['main']['shift'] if 'main' in clock else '!!MISSING!!',
                width = clock['main']['width'] if 'main' in clock else '!!MISSING!!',
                CLK_FLAGS = 'CLK_IS_CRITICAL' if is_critical else '0',
            )
            if clock['type'] == 'fixed_div':
                ffactors.append('\tFFACTOR(0, "{name}", "{parent}", 1, {factor}, 0),\n'.format(factor=clock['ratio'], **fmt))
            elif clock['type'] in ('pll_141xx', 'pll_1431x'): # XXX: is this correct?
                if is_critical:
                    plls.append('''\
\t__PLL(pll_1417x, CLK_{NAME}, "{name}", "oscclk", CLK_IS_CRITICAL,
\t    PLL_LOCKTIME_{REG}, PLL_CON0_{REG},
\t    NULL),
'''.format(**fmt))
                else:
                    plls.append('''\
\tPLL(pll_1417x, CLK_{NAME}, "{name}", "oscclk",
\t    PLL_LOCKTIME_{REG}, PLL_CON0_{REG},
\t    NULL),
'''.format(**fmt))
                reg_defs.append((clock['main']['offset'], 'PLL_CON0_'+reg_name))
                reg_defs.append((clock['status']['offset'], 'PLL_LOCKTIME_'+reg_name))
            elif clock['type'] == 'mux' and not clock.get('is_umux', False):
                muxes.append('''\
\tMUX(CLK_{NAME}, "{name}", {name}_p,
\t    CLK_CON_MUX_{REG}, {shift}, {width}),
'''.format(**fmt))
                reg_defs.append((clock['main']['offset'], 'CLK_CON_MUX_'+reg_name))
                mux_parents.append(('PNAME('+name+'_p)', [get_mainline_name(clocks[i], clock) for i in clock['parents']]))
            elif clock['type'] == 'div':
                divs.append('''\
\tDIV(CLK_{NAME}, "{name}", "{parent}",
\t    CLK_CON_DIV_{REG}, {shift}, {width}),
'''.format(**fmt))
                reg_defs.append((clock['main']['offset'], 'CLK_CON_DIV_'+reg_name))
            elif clock['type'] == 'gate' or (clock['type'] == 'mux' and clock.get('is_umux', False)):
                rg = clock['type'][:3].upper()
                gates.append('''\
\tGATE(CLK_{NAME}, "{name}", "{parent}",
\t    CLK_CON_{TYP}_{REG}, {shift}, {CLK_FLAGS}, 0),
'''.format(TYP=rg, **fmt))
                reg_defs.append((clock['main']['offset'], 'CLK_CON_'+rg+'_'+reg_name))
            else: assert False, clock['type']
        # print register definitions
        file.write('\n/* Register Offset definitions for '+bank_name+' ('+hex(bank_addrs[bank])+') */\n')
        reg_defs.sort()
        if reg_defs:
            max_len = (max(len(i[1]) for i in reg_defs) + 8) & -8
            for i, j in reg_defs:
                file.write('#define ' + j + '\t'*((max_len - len(j) + 7) >> 3) + '0x%04x\n'%i)
        # print fixed factor clocks
        if ffactors:
            file.write('\nstatic const struct samsung_fixed_factor_clock '+bank_name_lower+'_fixed_factor_clks[] __initconst = {\n')
            file.write(''.join(ffactors))
            file.write('};\n')
        # print an array of clk regs
        if reg_defs:
            file.write('\nstatic const unsigned long '+bank_name_lower+'_clk_regs[] __initconst = {\n')
            for i, j in reg_defs:
                file.write('\t'+j+',\n')
            file.write('};\n')
        # print pll clocks
        if plls:
            file.write('\nstatic const struct samsung_pll_clock '+bank_name_lower+'_pll_clks[] __initconst = {\n')
            file.write(''.join(plls))
            file.write('};\n')
        # print mux clock parent lists
        if mux_parents:
            file.write('\n/* List of parent clocks for Muxes in '+bank_name+' */\n')
            max_pos = (max(len(i[0]) for i in mux_parents) + 10) & -8
            for i, j in mux_parents:
                assert j
                file.write(i + '\t'*((max_pos - len(i) + 7) >> 3) + '= { ')
                for i in range(0, len(j)-2, 2):
                    file.write('"'+j[i]+'", "'+j[i+1]+'",\n'+'\t'*(max_pos>>3)+'    ')
                if len(j) % 2 == 0:
                    file.write('"'+j[-2]+'", ')
                file.write('"'+j[-1]+'" };\n')
        # print muxes
        if muxes:
            file.write('\nstatic const struct samsung_mux_clock '+bank_name_lower+'_mux_clks[] __initconst = {\n')
            file.write(''.join(muxes))
            file.write('};\n')
        # print div clocks
        if divs:
            file.write('\nstatic const struct samsung_div_clock '+bank_name_lower+'_div_clks[] __initconst = {\n')
            file.write(''.join(divs))
            file.write('};\n')
        # print gates
        if gates:
            file.write('\nstatic const struct samsung_gate_clock '+bank_name_lower+'_gate_clks[] __initconst = {\n')
            file.write(''.join(gates))
            file.write('};\n')
        # print bank summary
        file.write('\nstatic const struct samsung_cmu_info '+bank_name_lower+'_cmu_info __initconst = {\n')
        if ffactors:
            file.write('\t.fixed_factor_clks\t= '+bank_name_lower+'_fixed_factor_clks,\n')
            file.write('\t.nr_fixed_factor_clks\t= ARRAY_SIZE('+bank_name_lower+'_pll_clks),\n')
        if plls:
            file.write('\t.pll_clks\t\t= '+bank_name_lower+'_pll_clks,\n')
            file.write('\t.nr_pll_clks\t\t= ARRAY_SIZE('+bank_name_lower+'_pll_clks),\n')
        if muxes:
            file.write('\t.mux_clks\t\t= '+bank_name_lower+'_mux_clks,\n')
            file.write('\t.nr_mux_clks\t\t= ARRAY_SIZE('+bank_name_lower+'_mux_clks),\n')
        if divs:
            file.write('\t.div_clks\t\t= '+bank_name_lower+'_div_clks,\n')
            file.write('\t.nr_div_clks\t\t= ARRAY_SIZE('+bank_name_lower+'_div_clks),\n')
        if gates:
            file.write('\t.gate_clks\t\t= '+bank_name_lower+'_gate_clks,\n')
            file.write('\t.nr_gate_clks\t\t= ARRAY_SIZE('+bank_name_lower+'_gate_clks),\n')
        file.write('\t.nr_clk_ids\t\t= '+bank_name_lower.upper()+'_NR_CLK,\n')
        if reg_defs:
            file.write('\t.clk_regs\t\t= '+bank_name_lower+'_clk_regs,\n')
            file.write('\t.nr_clk_regs\t\t= ARRAY_SIZE('+bank_name_lower+'_clk_regs),\n')
        file.write('};\n\n')
        ## print init function
        #file.write('''
#static void __init {soc}_cmu_{bank}_init(struct device_node *np)
#{opening}
#\texynos_arm64_register_cmu(NULL, np, &{bank}_cmu_info);
#{closing}
#
#'''.format(soc=soc_name, bank=bank_name_lower, opening='{', closing='}'))
    # print probe function
    file.write('''\
/* ---- platform_driver ----------------------------------------------------- */
static int __init %(soc)s_cmu_probe(struct platform_device *pdev)
{
\tconst struct samsung_cmu_info *info;
\tstruct device *dev = &pdev->dev;

\tinfo = of_device_get_match_data(dev);
\texynos_arm64_register_cmu(dev, dev->of_node, info);

\treturn 0;
}
'''%dict(soc=soc_name))
    # print info table
    assert sorted_banks
    file.write('\nstatic const struct of_device_id '+soc_name+'_cmu_of_match[] = {\n\t')
    for i in sorted_banks:
        file.write('''\
{
\t\t.compatible = "samsung,%(soc)s-cmu-%(bank)s",
\t\t.data = &%(bank)s_cmu_info,
\t}, '''%dict(soc=soc_name, bank=get_mainline_bank_name(i, cmu=False).lower()))
    file.write('{\n\t},\n};\n')
    # print driver structure and initcall
    file.write('''
static struct platform_driver %(soc)s_cmu_driver __refdata = {
\t.driver = {
\t\t.name = "%(soc)s-cmu",
\t\t.of_match_table = %(soc)s_cmu_of_match,
\t\t.suppress_bind_attrs = true,
\t},
\t.probe = %(soc)s_cmu_probe,
};

static int __init %(soc)s_cmu_init(void)
{
\treturn platform_driver_register(&%(soc)s_cmu_driver);
}
core_initcall(%(soc)s_cmu_init);
'''%dict(soc=soc_name))

# step 8. print the devicetree header

with open(sys.argv[4], 'w') as file:
    file.write('''\
// SPDX-License-Identifier: GPL-2.0-only
/*
 * Copyright (C) '''+str(copyright_year)+''' Samsung Electronics Co., Ltd.
 * Copyright (C) '''+str(time.localtime().tm_year)+''' ???
 *
 * Device Tree binding constants for {Soc} clock controller.
 */

'''.format(soc=soc_name, Soc=soc_name[:1].upper()+soc_name[1:].lower()))
    soc_caps = soc_name.upper()
    i = 0
    while not soc_caps[i].isnumeric():
        i += 1
    soc_caps = soc_caps[:i]+'_'+soc_caps[i:]
    file.write('''\
#ifndef _DT_BINDINGS_CLOCK_{SOC}_H
#define _DT_BINDINGS_CLOCK_{SOC}_H
'''.format(SOC=soc_caps))
    for bank in sorted_banks:
        bank_name = get_mainline_bank_name(bank)
        file.write('\n/* '+bank_name+' */\n')
        names = [('CLK_'+get_mainline_name(clocks[clock], clocks[clock]).upper(), clock_location[clock][1]) for clock in clocks_per_bank[bank] if clock in clocks]
        names.append((get_mainline_bank_name(bank, cmu=False)+'_NR_CLK', len(clocks_per_bank[bank])+1))
        max_len = (max(len(i[0]) for i in names) + 8) & -8
        for i, j in names:
            file.write('#define ' + i + '\t'*((max_len - len(i) + 7) >> 3) + str(j) + '\n')
    file.write('''\

#endif /* _DT_BINDINGS_CLOCK_{SOC}_H */
'''.format(SOC=soc_caps))

# step 9. write dts source

with open(sys.argv[5], 'w') as file:
    if nr_devicetree >= 2:
        fixed_deps = set()
        for bank in sorted_banks:
            for i in bank_clk_deps[bank]:
                if i not in clock_location or clock_location[i][0] is None:
                    fixed_deps.add(i)
        for i in sorted(fixed_deps):
            if i not in clocks: continue
            clock = clocks[i]
            name = get_mainline_name(clock, clock)
            assert clock['type'] == 'fixed_rate'
            file.write('\t'+name+': '+name+' {\n')
            file.write('\t\tcompatible = "fixed-clock";\n')
            file.write('\t\t#clock-cells = <0>;\n\n')
            file.write('\t\tclock-frequency = <'+str(clock['rate'])+'>;\n')
            file.write('\t};\n\n')
    assert sorted_banks
    for i, bank in enumerate(sorted_banks):
        if i:
            file.write('\n')
        bank_name = get_mainline_bank_name(bank, cmu=False).lower()
        file.write('\tcmu_'+bank_name+': clock-controller@%X'%bank_addrs[bank]+' {\n')
        file.write('\t\tcompatible = "samsung,'+soc_name+'-cmu-'+bank_name+'";\n')
        bank_size = 0
        for i in clocks_per_bank[bank]:
            if i not in clocks:
                continue
            i = clocks[i]
            if 'main' in i:
                bank_size = max(bank_size, i['main']['offset'] + 4)
        file.write('\t\treg = <'+hex(bank_addrs[bank])+' '+hex(bank_size)+'>;\n')
        file.write('\t\t#clock-cells = <1>;\n')
        if bank_clk_deps[bank]:
            clock_array = []
            for i in bank_clk_deps[bank]:
                if i not in clocks: continue
                clock = clocks[i]
                name = get_mainline_name(clock, clock)
                if i in clock_location and clock_location[i][0] is not None:
                    bank_name = get_mainline_bank_name(clock_location[i][0], cmu=False).lower()
                    if nr_devicetree >= 1:
                        clock_array.append(('<&cmu_'+bank_name+' '+str(clock_location[i][1])+'>', name))
                    else:
                        clock_array.append(('<&cmu_'+bank_name+' CLK_'+name.upper()+'>', name))
                else:
                    clock_array.append(('<&'+name+'>', name))
            if clock_array:
                file.write('\n\t\tclocks = ')
                for i, j in clock_array[:-1]:
                    file.write(i+',\n\t\t\t ')
                file.write(clock_array[-1][0]+';\n')
                file.write('\t\tclock-names = ')
                for i, j in clock_array[:-1]:
                    file.write('"'+j+'",\n\t\t\t      ')
                file.write('"'+clock_array[-1][1]+'";\n')
        file.write('\t};\n')
