// SPDX-License-Identifier: GPL-2.0
/*
 * Samsung Galaxy Tab A 10.1 2016 Wi-Fi (gtaxlwifi) device tree source
 *
 * Copyright (c) ???
 */

/dts-v1/;
#include "exynos7870.dtsi"
#include <dt-bindings/gpio/gpio.h>
#include <dt-bindings/input/input.h>
#include <dt-bindings/interrupt-controller/irq.h>

/ {
	model = "Samsung Galaxy Tab A 10.1 (2016) Wi-Fi";
	compatible = "samsung,gtaxlwifi", "samsung,exynos7870";
	chassis-type = "tablet";

	chosen {
	};

	reserved-memory {
		#address-cells = <2>;
		#size-cells = <1>;
		ranges;

		bootloaderfb@67000000 {
			reg = <0 0x67000000 0x8ca000>;
			no-map;
		};
	};

	memory@40000000 {
		device_type = "memory";
		reg = <0 0x40000000 0x3e400000>;
	};

	memory@80000000 {
		device_type = "memory";
		reg = <0 0x80000000 0x40000000>;
	};

	bootloaderfb@67000000 {
		compatible = "simple-framebuffer";
		reg = <0 0x67000000 (1920*1200*4)>;
		width = <1200>;
		height = <1920>;
		stride = <(1200*4)>;
		format = "a8r8g8b8";
	};
	vdd_fixed_touchscreen: fixedregulator-toucscreen {
		compatible = "regulator-fixed";
		regulator-name = "vdd_fixed_touchscreen";
		regulator-max-microvolt = <1800000>;
		regulator-min-microvolt = <1800000>;

		gpio = <&gpd1 4 GPIO_ACTIVE_HIGH>;
		enable-active-high;
	};
	avdd_fixed_touchscreen: fixedregulator-toucscreen-a {
		compatible = "regulator-fixed";
		regulator-name = "avdd_fixed_touchscreen";
		regulator-max-microvolt = <3300000>;
		regulator-min-microvolt = <3300000>;

		gpio = <&gpd1 6 GPIO_ACTIVE_HIGH>;
		enable-active-high;
	};
	
	
	pwrseq_mmc1: pwrseq-mmc1 {
		compatible = "mmc-pwrseq-simple";
		reset-gpios = <&gpd3 6 GPIO_ACTIVE_LOW>;
	};

	vdd_fixed_wifi: vdd_fixed_wlan {
		compatible = "regulator-fixed";
		regulator-name = "cnss_dcdc_en";
		startup-delay-us = <4000>;
		gpio = <&gpa0 6 GPIO_ACTIVE_HIGH>;
		enable-active-high;
	};
	
	vdd_fixed_mmc2: regulator-fixed-mmc2 {
		compatible = "regulator-fixed";
		regulator-name = "vdd_fixed_mmc2";
		regulator-max-microvolt = <2800000>;
		regulator-min-microvolt = <2800000>;

		gpio = <&gpc0 0 GPIO_ACTIVE_HIGH>;
		enable-active-high;
	};
	
};

&pinctrl0 {
	pmic_irq: pmic-irq {
		samsung,pins = "gpa0-2";
		samsung,pin-pud = <EXYNOS_PIN_PULL_UP>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR4>;
	};
	
	wlan_hostwake: wlan-hostwake-pins {
		samsung,pins = "gpa2-2";
		samsung,pin-function = <EXYNOS_PIN_FUNC_OUTPUT>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_UP>;
	};

		muic_irq: muic_irq {
			samsung,pins = "gpa2-6";
			samsung,pin-function = <EXYNOS_PIN_FUNC_INPUT>;
			samsung,pin-pud = <EXYNOS_PIN_PULL_UP>;
		};
	
	
	dwmmc2_irq: dwmmc2-irq-pins {
		samsung,pins = "gpa0-1";
		samsung,pin-function = <EXYNOS_PIN_FUNC_EINT>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR1>;
	};



};

&pinctrl6 {
	pm_wrsti: pm-wrsti {
		samsung,pins = "gpd1-0";
		samsung,pin-con-pdn = <EXYNOS_PIN_PDN_PREV>;
	};

	wlan_enable: wlan-enable-pins {
		samsung,pins = "gpd3-6";
		samsung,pin-function = <EXYNOS_PIN_FUNC_OUTPUT>;
		samsung,pin-con-pdn = <EXYNOS_PIN_PDN_PREV>;
		samsung,pin-pud-pdn = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-pud = <EXYNOS_PIN_PULL_NONE>;
		samsung,pin-drv = <EXYNOS5433_PIN_DRV_FAST_SR4>;
		samsung,pin-val = <0>;
	};

};

&hsi2c0 {
	#address-cells = <1>;
	#size-cells = <0>;
	status = "okay";

	sda-gpios = <&gpm0 0 GPIO_ACTIVE_HIGH>;
	scl-gpios = <&gpm0 1 GPIO_ACTIVE_HIGH>;
	i2c-gpio,delay-us = <2>;

	s2mpu05-pmic@66 {
		compatible = "samsung,s2mpu05-pmic";
		reg = <0x66>;
		interrupt-parent = <&gpa0>;
		interrupts = <2 IRQ_TYPE_LEVEL_LOW>;

		pinctrl-names = "default";
		pinctrl-0 = <&pmic_irq &pm_wrsti>;

		regulators {
			BUCK1 {
				regulator-name = "BUCK1";
				regulator-min-microvolt = <500000>;
				regulator-max-microvolt = <1300000>;
				regulator-ramp-delay = <12000>;

				regulator-expected-consumer = <2>;

				regulator-boot-on;
				regulator-always-on;
			};

			BUCK2 {
				regulator-name = "BUCK2";
				regulator-min-microvolt = <500000>;
				regulator-max-microvolt = <1300000>;
				regulator-ramp-delay = <12000>;

				regulator-expected-consumer = <4>;

				regulator-boot-on;
				regulator-always-on;
			};

			BUCK3 {
				regulator-name = "BUCK3";
				regulator-min-microvolt = <500000>;
				regulator-max-microvolt = <1300000>;
				regulator-ramp-delay = <12000>;

				regulator-boot-on;
				regulator-always-on;
			};

			BUCK4 {
				regulator-name = "BUCK4";
				regulator-min-microvolt = <1200000>;
				regulator-max-microvolt = <1500000>;
				regulator-ramp-delay = <12000>;

				regulator-boot-on;
				regulator-always-on;
			};

			BUCK5 {
				regulator-name = "BUCK5";
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <2100000>;
				regulator-ramp-delay = <12000>;

				regulator-boot-on;
				regulator-always-on;
			};

			vdd_ldo1: LDO1 {
				regulator-name = "vdd_ldo1";
				regulator-min-microvolt = <650000>;
				regulator-max-microvolt = <1350000>;
				regulator-ramp-delay = <12000>;

				regulator-boot-on;
				regulator-always-on;
			};

			vdd_ldo2: LDO2 {
				regulator-name = "vdd_ldo2";
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <2800000>;
				regulator-ramp-delay = <12000>;
			};

			vdd_ldo3: LDO3 {
				regulator-name = "vdd_ldo3";
				regulator-min-microvolt = <800000>;
				regulator-max-microvolt = <2375000>;
				regulator-ramp-delay = <12000>;

				regulator-boot-on;
				regulator-always-on;
			};

			vdd_ldo4: LDO4 {
				regulator-name = "vdd_ldo4";
				regulator-min-microvolt = <800000>;
				regulator-max-microvolt = <1350000>;
				regulator-ramp-delay = <12000>;

				regulator-boot-on;
				regulator-always-on;
			};

			vdd_ldo5: LDO5 {
				regulator-name = "vdd_ldo5";
				regulator-min-microvolt = <800000>;
				regulator-max-microvolt = <1350000>;
				regulator-ramp-delay = <12000>;

				regulator-boot-on;
				regulator-always-on;
			};

			vdd_ldo6: LDO6 {
				regulator-name = "vdd_ldo6";
				regulator-min-microvolt = <800000>;
				regulator-max-microvolt = <1350000>;
				regulator-ramp-delay = <12000>;

				regulator-boot-on;
				regulator-always-on;
			};

			vdd_ldo7: LDO7 {
				regulator-name = "vdd_ldo7";
				regulator-min-microvolt = <800000>;
				regulator-max-microvolt = <2375000>;
				regulator-ramp-delay = <12000>;

				regulator-boot-on;
				regulator-always-on;
			};

			vdd_ldo8: LDO8 {
				regulator-name = "vdd_ldo8";
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <3375000>;
				regulator-ramp-delay = <12000>;

				regulator-boot-on;
				regulator-always-on;
			};

			vdd_ldo9: LDO9 {
				regulator-name = "vdd_ldo9";
				regulator-min-microvolt = <650000>;
				regulator-max-microvolt = <1350000>;
				regulator-ramp-delay = <12000>;

				regulator-always-on;
			};

			vdd_ldo10: LDO10 {
				regulator-name = "vdd_ldo10";
				regulator-min-microvolt = <650000>;
				regulator-max-microvolt = <1350000>;
				regulator-ramp-delay = <12000>;

				regulator-always-on;
			};

			vdd_ldo25: LDO25 {
				regulator-name = "vdd_ldo25";
				regulator-min-microvolt = <1200000>;
				regulator-max-microvolt = <1200000>;
			};

			vdd_ldo26: LDO26 {
				regulator-name = "vdd_ldo26";
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <3375000>;
				regulator-ramp-delay = <12000>;

				regulator-boot-on;

			};

			vdd_ldo27: LDO27 {
				regulator-name = "vdd_ldo27";
				regulator-min-microvolt = <800000>;
				regulator-max-microvolt = <2375000>;
				regulator-ramp-delay = <12000>;

				regulator-boot-on;
			};

			vdd_ldo29: LDO29 {
				regulator-name = "vdd_ldo29";
				regulator-min-microvolt = <2800000>;
				regulator-max-microvolt = <2800000>;
			};

			vdd_ldo30: LDO30 {
				regulator-name = "vdd_ldo30";
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <1800000>;

				regulator-always-on;
			};

			vdd_ldo31: LDO31 {
				regulator-name = "vdd_ldo31";
				regulator-min-microvolt = <2800000>;
				regulator-max-microvolt = <2800000>;
				regulator-ramp-delay = <12000>;

				regulator-boot-on;
				regulator-always-on;
			};

			vdd_ldo32: LDO32 {
				regulator-name = "vdd_ldo32";
				regulator-min-microvolt = <3300000>;
				regulator-max-microvolt = <3300000>;

				regulator-always-on;
			};

			vdd_ldo33: LDO33 {
				regulator-name = "vdd_ldo33";
				regulator-min-microvolt = <2800000>;
				regulator-max-microvolt = <2800000>;
				regulator-boot-on;
				regulator-always-on;
			};

			vdd_ldo34: LDO34 {
				regulator-name = "vdd_ldo34";
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <1800000>;
			};

			vdd_ldo35: LDO35 {
				regulator-name = "vdd_ldo35";
				regulator-min-microvolt = <1800000>;
				regulator-max-microvolt = <1800000>;

				regulator-boot-on;
				regulator-always-on;
			};
		};
	};
};

&gpu {
	status = "okay";
};

&dwmmc0 {
	status = "okay";
	#address-cells = <1>;
	#size-cells = <0>;
	non-removable;

	vmmc-supply = <&vdd_ldo26>;
	vqmmc-supply = <&vdd_ldo27>;

	fifo-depth = <64>;
	samsung,dw-mshc-ciu-div = <3>;
	samsung,dw-mshc-sdr-timing = <0 4>;
	samsung,dw-mshc-ddr-timing = <2 4>;
};


&dwmmc1 {
	status = "okay";
	#address-cells = <1>;
	#size-cells = <0>;

	mmc-pwrseq = <&pwrseq_mmc1>;

	pinctrl-names = "default";
	pinctrl-0 = <&sd1_clk &sd1_cmd &sd1_bus1 &sd1_bus4>;

	bus-width = <4>;
	fifo-depth = <64>;
	samsung,dw-mshc-ciu-div = <3>;
	samsung,dw-mshc-sdr-timing = <0 3>;
	samsung,dw-mshc-ddr-timing = <1 2>;
	non-removable;
	cap-sd-highspeed;
	cap-sdio-irq;
	pio-mode;

	wifi@1 {
		compatible = "qcom,ath10k";
		reg = <0x1>;

		interrupt-names = "host-wake";
		interrupt-parent = <&gpa2>;
		interrupts = <2 IRQ_TYPE_LEVEL_LOW>;

		vdd-0.8-cx-mx-supply = <&vdd_fixed_wifi>;

		resets = <&gpd3 6 GPIO_ACTIVE_LOW>;
        };
};

&dwmmc2 {
	status = "okay";

	pinctrl-names = "default";
	pinctrl-0 = <&sd2_clk &sd2_cmd &sd2_bus1 &sd2_bus4 &dwmmc2_irq>;

	vmmc-supply = <&vdd_fixed_mmc2>;
	vqmmc-supply = <&vdd_ldo2>;

	bus-width = <4>;
	fifo-depth = <64>;
	samsung,dw-mshc-ciu-div = <3>;
	samsung,dw-mshc-sdr-timing = <0 3>;
	samsung,dw-mshc-ddr-timing = <1 2>;
	samsung,dw-mshc-sdr50-timing = <2 4>;
	samsung,dw-mshc-sdr104-timing = <0 3>;
	non-removable;
	sd-uhs-sdr50;
	sd-uhs-sdr104;
	disable-wp;
};






&usbdrd {
	status = "okay";
};
&i2c3 {
	#address-cells = <1>;
	#size-cells = <0>;
	status = "okay";

	samsung,i2c-max-bus-freq = <400000>;
	samsung,i2c-sda-delay = <100>;
	
	touchscreen@49 {
			compatible = "st,stmfts";
			reg = <0x49>;
			interrupt-parent = <&gpa0> ;
			interrupts = <4 IRQ_TYPE_LEVEL_LOW>;
			touchscreen-size-x = <1199>;
			touchscreen-size-y = <1919>;
			avdd-supply = <&avdd_fixed_touchscreen>;
			vdd-supply = <&vdd_fixed_touchscreen>;
			/*touch-key-connected;*/
		};
};
&i2c5 {
	#address-cells = <1>;
	#size-cells = <0>;
	status = "okay";
	
	cm3323-i2c@10 {
			compatible = "capella,cm3323";
			reg = <0x10>;
			vdd-supply = <&vdd_ldo33>;
		};
};

&i2c7{
	#address-cells = <1>;
	#size-cells = <0>;
	status = "okay";
	
	muic: extcon@25 {
			compatible = "siliconmitus,sm5703-muic";
			reg = <0x25>;
			interrupt-parent = <&gpa2>;
			interrupts = <6 IRQ_TYPE_EDGE_FALLING>;
			pinctrl-names = "default";
			pinctrl-0 = <&muic_irq>;
			
		};
};


